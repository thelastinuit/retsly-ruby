require "spec_helper"

RSpec.describe Retsly::Datasets do
  before do
    Retsly.vendor = 'test'
    Retsly.access_token = 'access_token'
  end

  describe ".retrieve" do 
    it "calls out to the listings API" do
      body = {
        "success" => true,
        "status" => 200,
        "bundle" => [
          {
            "id" => "85ee39ac-bf1f-4000-ac53-2481d9299859",
            "datasetCode" => "internal_fgcmls",
            "name" => "Florida Gulf Coast MLS",
            "description" => "",
            "info"=> "",
            "logo" => nil,
            "state" => "",
            "location" => "",
            "test" => false,
            "hidden" => false,
            "internal" => true,
            "type" => "mls",
            "center" => nil,
            "boundary" => nil,
            "createdAt" => "2017-08-31T18:47:51.940Z",
            "updatedAt" => "2017-08-31T18:47:51.940Z",
            "parentID" => "dbebe487-aab2-4773-bc9c-afe33299a8fb",
            "vendorID" => "internal_fgcmls"
          }
        ]
      }

      stub_request(:get, 'https://rets.io/api/v1/datasets?access_token=access_token').
        to_return(:status => 200, :body => body.to_json, headers: {'Content-Type' => 'application/json'})

      Retsly::Datasets.retrieve
    end
  end
end
