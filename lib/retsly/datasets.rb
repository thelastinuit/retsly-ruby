module Retsly
  class Datasets
    def self.retrieve
      Retsly::VendorlessConnection.get("datasets")
    end
  end
end
