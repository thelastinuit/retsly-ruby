module Retsly
  class Offices
    def self.retrieve(params)
      Retsly::Connection.get("offices", params)
    end
  end
end
