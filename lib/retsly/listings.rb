module Retsly
  class Listings
    def self.retrieve(params)
      Retsly::Connection.get("listings", params)
    end
  end
end
