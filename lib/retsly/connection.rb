require 'net/http'
require 'net/https'
require 'json'

module Retsly
  class Connection
    def self.get(endpoint, params = nil)
      request_url = nil
      case endpoint
      when "offices"
        request_url = "/api/v2/#{Retsly.vendor}/#{endpoint}/#{params["OfficeId"]}?access_token=#{Retsly.access_token}"
      else
        request_url = "/api/v2/#{Retsly.vendor}/#{endpoint}?access_token=#{Retsly.access_token}"
        request_url += params if params.present?  
      end  
      
      response = nil

      response = if defined? APICache && APICache.store.present?
                   begin
                     APICache.get(request_url) do
                       request request_url
                     end
                   rescue APICache::APICacheError
                     OpenStruct.new(body: { "success" => false }.to_json)
                   end
                 else
                   request request_url
                 end

      JSON.parse(response.body)
    end

    private

    def self.request(request_url)
      http = Net::HTTP.new("rets.io", 443)
      configure_ssl(http)
      http.open_timeout = Retsly.open_timeout
      http.read_timeout = Retsly.read_timeout

      http.get(request_url, {'Accept' => 'application/json'})
    end

    def self.configure_ssl(http)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER

      http
    end
  end
end
